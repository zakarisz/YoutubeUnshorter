// ==UserScript==
// @name     YoutubeUnshorter
// @namespace https://codeberg.org/zakarisz
// @version  1.1.0
// @grant    none
// @match    https://*.youtube.com/*
// @description Hide shorts on youtube
// ==/UserScript==

function hide_shorts() {
  // Remove Shorts in the feed
	items = document.querySelectorAll(".ytd-rich-grid-row > ytd-rich-item-renderer");
  for (let i = 0; i < items.length-1; i++) {
    let a = items[i].getElementsByTagName('a');
    for (let j = 0; j < a.length-1; j++) {
      if (a[j].href.includes('shorts/')) {
        console.log("Hide short")
        items[i].style = "display:none";
        break;
      }
    }
  }

  // Remove Shorts entry in the menu
  menus = document.querySelectorAll("#items > ytd-guide-entry-renderer");
  for (let i = 0; i < menus.length-1; i++) {
    let a = menus[i].getElementsByTagName('a');
    if (a.length > 0 && a[0].getAttribute("title") == "Shorts") {
      menus[i].style = "display:none";
      break;
    }
  }
}


// Run once upon page loading, then run periodically to catch newly-loaded content
setTimeout(hide_shorts, 400)
setInterval(hide_shorts, 5000)
